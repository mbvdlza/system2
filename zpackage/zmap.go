package zpackage

import (
	"sync"
	"fmt"
)

func Zmap2(c []string, applyFuncX func(string, *chan string, *sync.WaitGroup), out *chan string, wg *sync.WaitGroup) {
	for elem := range c {
		fmt.Printf("Adding: \t%s\n", c[elem])
		go applyFuncX(c[elem], out, wg)
	}
}
