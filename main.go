package main

import (
	"bitbucket.org/mbvdlza/system2/zpackage"
	"fmt"
	"strings"
	"sync"
)

func main() {
	// the slice to work on!
	inSlice := []string{"jaco", "dEen", "z4c", "mike", "PICKERING", "dude", "m@rlon", " ", "8888tt888"}

	var bufferSize int = len(inSlice)
	out := make(chan string, bufferSize)

	var wg sync.WaitGroup
	wg.Add(bufferSize)

	zpackage.Zmap2(inSlice, capsString2, &out, &wg)

	fmt.Println("Added all work, waiting......")
	wg.Wait()
	fmt.Println("Done waiting. Old vs new:")
	fmt.Println(inSlice)
	fmt.Println(output(&bufferSize, &out))
}

// This function is the callable sent to map
func capsString2(s string, o *chan string, wg *sync.WaitGroup) {
	defer wg.Done()
	fmt.Printf("Working:\t%s\n", s)
	*o <- strings.ToUpper(s)
	fmt.Printf("Done:\t\t%s\n", strings.ToUpper(s))
}

// Rebuild the new output array, from channel data
// Split out of main for logical breakdown :)
func output(buf *int, ch *chan string) []string{
	var outSlice []string
	for i := 0; i < *buf; i++ {
		outSlice = append(outSlice, <-*ch)
	}
	return outSlice
}